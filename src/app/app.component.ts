import { Component } from '@angular/core';

@Component({
  selector: 'mytweempus-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mytweempus works!';
}
