import { MyTweempusPage } from './app.po';

describe('my-tweempus App', () => {
  let page: MyTweempusPage;

  beforeEach(() => {
    page = new MyTweempusPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('mytweempus works!');
  });
});
