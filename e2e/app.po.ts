import { browser, element, by } from 'protractor';

export class MyTweempusPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mytweempus-root h1')).getText();
  }
}
